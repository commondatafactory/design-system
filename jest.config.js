// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
module.exports = {
  setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
  collectCoverageFrom: [
    'src/components/**/*.js',
    '!src/components/**/*.stories.js',
  ],
  coverageReporters: ['text', 'html'],
  moduleNameMapper: {
    '\\.css$': 'identity-obj-proxy',
    '\\.svg$': '<rootDir>/__mocks__/svgrMock.js',
  },
  testEnvironment: 'jsdom',
  testMatch: ['<rootDir>/src/**/*.test.js'],
}
