# Common Ground - Design System

[View Demo](https://commondatafactory.gitlab.io/design-system)

# Documentation

- [Development Setup](./docs/01-development-setup.md)
- [Release Process](./docs/02-release-process.md)
- [Preview changes in dependant applications](./docs/03-preview-changes-in-dependent-apps.md)

## How to contribute

See [CONTRIBUTING](./CONTRIBUTING.md)
