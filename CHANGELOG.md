## [0.2.4](https://gitlab.com/commondatafactory/design-system/compare/v0.2.3...v0.2.4) (2024-06-24)


### Bug Fixes

* **config:** resolve a11y issue for input with actionable icon ([ab46636](https://gitlab.com/commondatafactory/design-system/commit/ab46636bdd7c3593f3b3392e7a16c313b7ede719))

## [0.2.3](https://gitlab.com/commondatafactory/design-system/compare/v0.2.2...v0.2.3) (2024-06-24)


### Bug Fixes

* **config:** resolve a11y issue for input with actionable icon ([1547e8c](https://gitlab.com/commondatafactory/design-system/commit/1547e8c832cbaef98b0c9b818955470f0a359f60))

## [0.2.2](https://gitlab.com/commondatafactory/design-system/compare/v0.2.1...v0.2.2) (2024-06-24)


### Bug Fixes

* **config:** resolve typo in url ([765b442](https://gitlab.com/commondatafactory/design-system/commit/765b442d7078447e2eed8281e02b0e9bb159d420))

## [0.2.1](https://gitlab.com/commondatafactory/design-system/compare/v0.2.0...v0.2.1) (2024-06-24)

# [0.2.0](https://gitlab.com/commondatafactory/design-system/compare/v0.1.0...v0.2.0) (2024-06-24)


### Bug Fixes

* **config:** add seemingly missing semantic release packages ([32af734](https://gitlab.com/commondatafactory/design-system/commit/32af734722c054a5b86a3ffa03ce50ec63696182))
* **config:** delete changelog file ([253fb15](https://gitlab.com/commondatafactory/design-system/commit/253fb155fac83f404edc9465e816c7d5c8384dd3))
* **config:** fix flag for publishing ([9c96ade](https://gitlab.com/commondatafactory/design-system/commit/9c96ade3ec0e68511e72f29bd55470afbf6ba50b))
* **config:** update semantic release ([33d235a](https://gitlab.com/commondatafactory/design-system/commit/33d235a21db884b4f141707e8f8f9d85b83d2830))


### Features

* **config:** publish to npm ([22b4ebd](https://gitlab.com/commondatafactory/design-system/commit/22b4ebddd5b16f64d7efd57915ef16a36cfb41b2))
* **config:** reinstate debug ([5346f96](https://gitlab.com/commondatafactory/design-system/commit/5346f96a16d9fa7a06c11d21890e7f158a0a0c0f))

# [0.2.0](https://gitlab.com/commondatafactory/design-system/compare/v0.1.0...v0.2.0) (2024-06-24)


### Bug Fixes

* **config:** add seemingly missing semantic release packages ([32af734](https://gitlab.com/commondatafactory/design-system/commit/32af734722c054a5b86a3ffa03ce50ec63696182))
* **config:** delete changelog file ([253fb15](https://gitlab.com/commondatafactory/design-system/commit/253fb155fac83f404edc9465e816c7d5c8384dd3))
* **config:** fix flag for publishing ([9c96ade](https://gitlab.com/commondatafactory/design-system/commit/9c96ade3ec0e68511e72f29bd55470afbf6ba50b))
* **config:** update semantic release ([33d235a](https://gitlab.com/commondatafactory/design-system/commit/33d235a21db884b4f141707e8f8f9d85b83d2830))


### Features

* **config:** publish to npm ([22b4ebd](https://gitlab.com/commondatafactory/design-system/commit/22b4ebddd5b16f64d7efd57915ef16a36cfb41b2))
* **config:** reinstate debug ([5346f96](https://gitlab.com/commondatafactory/design-system/commit/5346f96a16d9fa7a06c11d21890e7f158a0a0c0f))

# [0.2.0](https://gitlab.com/commondatafactory/design-system/compare/v0.1.0...v0.2.0) (2024-06-05)


### Bug Fixes

* **config:** add seemingly missing semantic release packages ([32af734](https://gitlab.com/commondatafactory/design-system/commit/32af734722c054a5b86a3ffa03ce50ec63696182))
* **config:** delete changelog file ([253fb15](https://gitlab.com/commondatafactory/design-system/commit/253fb155fac83f404edc9465e816c7d5c8384dd3))
* **config:** fix flag for publishing ([9c96ade](https://gitlab.com/commondatafactory/design-system/commit/9c96ade3ec0e68511e72f29bd55470afbf6ba50b))
* **config:** update semantic release ([33d235a](https://gitlab.com/commondatafactory/design-system/commit/33d235a21db884b4f141707e8f8f9d85b83d2830))


### Features

* **config:** publish to npm ([22b4ebd](https://gitlab.com/commondatafactory/design-system/commit/22b4ebddd5b16f64d7efd57915ef16a36cfb41b2))
* **config:** reinstate debug ([5346f96](https://gitlab.com/commondatafactory/design-system/commit/5346f96a16d9fa7a06c11d21890e7f158a0a0c0f))
