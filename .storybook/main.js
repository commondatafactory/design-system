import remarkGfm from "remark-gfm";

module.exports = {
  stories: [
      '../src/**/*.mdx',
      '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-a11y',
    '@storybook/addon-themes',
    '@chromatic-com/storybook',
    {
      name: '@storybook/addon-docs',
      options: {
        mdxPluginOptions: {
          mdxCompileOptions: {
            remarkPlugins: [remarkGfm],
          },
        },
      },
    },
    '@storybook/addon-webpack5-compiler-swc'
  ],
  core: {
    // builder: '@storybook/builder-vite',
    disableTelemetry: true
  },
  framework: '@storybook/react-webpack5',
  // framework: {
    // name: '@storybook/react-vite',
    // options: {},
  // },
  docs: {
    autodocs: true
  },
  webpackFinal: config => {
    const fileLoaderRule = config.module.rules.find(rule => rule.test && rule.test.test('.svg'));
    fileLoaderRule.exclude = /\.svg$/;

    config.module.rules.push({
      test: /\.svg$/,
      enforce: 'pre',
      loader: require.resolve('@svgr/webpack'),
    });

    return config;
  }
};
