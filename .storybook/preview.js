
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'
import { GlobalStyles } from '../src'
import {ThemeProvider} from "styled-components";
import {defaultTheme} from '../src/themes'

import { withThemeFromJSXProvider } from '@storybook/addon-themes';

export const decorators = [
  withThemeFromJSXProvider({
  themes: {
    default: defaultTheme
  },
  Provider: ThemeProvider,
  GlobalStyles,
})];

export const parameters = {
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
}
