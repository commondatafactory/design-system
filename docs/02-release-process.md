Release Process
===============

# Conventions

The Release Process is based on [Semantic Release](https://semantic-release.gitbook.io/semantic-release/). 
We use the [Angular Git Commit conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)).

Because of this:

* contributors are forced to think about their commits & commit messages
* the release process is fully automated
* the documentation of the [releases](../CHANGELOG.md) is generated automatically

# Workflow

1. Generate a new branch from `main`, prefixed by `feature/<issue-number>`. Eg. `feature/390-my-feature`.
1. Push your work to the feature branch. Make sure your commit messages are written using the conventions.
1. Create a Merge Request (MR) and assign at lease one person to review your work.
1. Once the MR is approved, you can merge the branch.

A new release will automatically be generated and published containing your additions.

# Useful links 

* [Example commit messages](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular)
