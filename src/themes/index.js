// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export { default as defaultTheme } from './default'
export { default as darkTheme } from './dark'
