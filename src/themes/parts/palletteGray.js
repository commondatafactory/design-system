// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
const palletteGray = {
  colorPaletteGray50: '#FAFAFA',
  colorPaletteGray100: '#F5F5F5',
  colorPaletteGray200: '#EEEEEE',
  colorPaletteGray300: '#E0E0E0',
  colorPaletteGray400: '#BDBDBD',
  colorPaletteGray500: '#9E9E9E',
  colorPaletteGray600: '#757575',
  colorPaletteGray700: '#616161',
  colorPaletteGray800: '#424242',
  colorPaletteGray900: '#212121',
}

export default palletteGray
