// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import { bool, string, node, oneOfType, shape, oneOf } from 'prop-types'
import { useFormContext } from 'react-hook-form'
import { Label, FieldLabel } from '../index'
import Input from './Input'
import { StyledLabel } from './index.styles'

const RadioGroup = ({ children, label, ...props }) => {
  return (
    <>
      <Label {...props}>{label}</Label>
      {children}
    </>
  )
}

RadioGroup.propTypes = {
  children: node.isRequired,
  label: string.isRequired,
}

RadioGroup.defaultProps = {
  disabled: false,
}

const Radio = ({ children, ...props }) => {
  const { register } = useFormContext()
  const { disabled, name } = props

  return (
    <StyledLabel disabled={disabled}>
      <Input type="radio" {...register(name)} {...props} />
      {children}
    </StyledLabel>
  )
}

Radio.Group = RadioGroup

Radio.propTypes = {
  children: oneOfType([string, shape({ type: oneOf([FieldLabel]) })])
    .isRequired,
  name: string.isRequired,
  value: string.isRequired,
  disabled: bool,
}

Radio.defaultProps = {
  disabled: false,
}

export default Radio
