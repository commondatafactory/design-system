// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React from 'react'
import { FormProvider, useForm } from 'react-hook-form'

import { Fieldset } from '../../Form'
import Radio from './index'

const radioStory = {
  title: 'Components/ReactHookForm/Radio',
  parameters: {
    componentSubtitle:
      'Form component to work with radio buttons. Batteries included (label & input).',
  },
  component: Radio,
}

export default radioStory

export const Intro = () => {
  const methods = useForm({
    defaultValues: {
      areYouOk: 'yes',
    },
  })

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <Radio.Group label="Are you ok?">
            <Radio name="areYouOk" value="yes">
              Yes
            </Radio>
            <Radio name="areYouOk" value="no">
              No
            </Radio>
          </Radio.Group>
        </form>
      </FormProvider>
    </>
  )
}

export const Disabled = () => {
  const methods = useForm({
    defaultValues: {
      areYouOk: 'yes',
      pizza: 'maybe',
    },
  })

  return (
    <>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(() => {})}>
          <Fieldset>
            <Radio.Group label="Are you ok?">
              <Radio name="areYouOk" value="yes" disabled>
                Yes
              </Radio>
              <Radio name="areYouOk" value="no" disabled>
                No
              </Radio>
            </Radio.Group>
          </Fieldset>

          <Fieldset>
            <Radio.Group label="Do you like pizza?">
              <Radio name="pizza" value="yes">
                Yes
              </Radio>
              <Radio name="pizza" value="no">
                No
              </Radio>
              <Radio name="pizza" value="maybe" disabled>
                Maybe
              </Radio>
            </Radio.Group>
          </Fieldset>
        </form>
      </FormProvider>
    </>
  )
}
