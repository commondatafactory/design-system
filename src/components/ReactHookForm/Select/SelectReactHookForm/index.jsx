// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//
import React, { forwardRef } from 'react'
import {
  arrayOf,
  object,
  shape,
  string,
  bool,
  oneOfType,
  func,
} from 'prop-types'
import { useFormContext } from 'react-hook-form'

import SelectComponent from '../SelectComponent'

/* eslint-disable react/prop-types */
/* prop-types not supported for forwardRef components */
export const SelectComponentWithRef = ({ ...props }, ref) => {
  const { options, isMulti, onChange, name, value } = props
  const { setValue } = useFormContext()
  const reactHookFormRelated = {}

  // Extract chosen value and pass to ReactHookForm
  reactHookFormRelated.onChange = (selectedOption, actionType) => {
    const getOptionValue =
      props.getOptionValue ||
      function (option) {
        return option.value
      }

    if (selectedOption === null) {
      setValue(name, isMulti ? [] : '')
    } else {
      setValue(
        name,
        isMulti
          ? selectedOption.map((option) => getOptionValue(option.value))
          : getOptionValue(selectedOption.value),
      )
    }

    if (onChange) {
      onChange(selectedOption, actionType)
    }
  }

  // Loop back the selection to react-select using it's value stored in ReactHookForm
  reactHookFormRelated.value = (() => {
    if (options) {
      return isMulti
        ? options.filter((option) =>
            Array.isArray(value)
              ? value
                  .map((option) => option.value)
                  .some(
                    (value) =>
                      JSON.stringify(value) === JSON.stringify(option.value),
                  )
              : JSON.stringify(value.value) === JSON.stringify(option.value),
          )
        : options.find((option) => {
            return JSON.stringify(option.value) === JSON.stringify(value?.value)
          })
    } else {
      return isMulti ? [] : ''
    }
  })()

  return <SelectComponent ref={ref} {...props} {...reactHookFormRelated} />
}

const SelectReactHookForm = forwardRef(SelectComponentWithRef)

SelectReactHookForm.propTypes = {
  options: arrayOf(
    shape({
      value: oneOfType([string, object]),
      label: string,
    }),
  ).isRequired,
  isMulti: bool,
  onChange: func,
  name: string,
}

export default SelectReactHookForm
