// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import React from 'react'
import * as Yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { FormProvider, useForm } from 'react-hook-form'
import { Select, SelectReactHookForm, SelectComponent } from './index'

const selectStory = {
  title: 'Components/ReactHookForm/Select',
  parameters: {
    componentSubtitle: 'Select component.',
  },
  component: Select,
}

export default selectStory

export const Intro = () => {
  const methods = useForm()

  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option">
          Select option
        </Select>
      </form>
    </FormProvider>
  )
}

export const Validation = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const schema = Yup.object().shape({
    option: Yup.mixed()
      .oneOf([options[0]], 'Option one is the only valid choice.')
      .required('Option one is the only valid choice.'),
  })

  const methods = useForm({
    mode: 'all',
    resolver: yupResolver(schema),
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option">
          Select option
        </Select>
      </form>
    </FormProvider>
  )
}

export const Size = () => {
  const methods = useForm()

  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="xs" size="xs">
          Extra small
        </Select>

        <Select options={options} name="s" size="s">
          Small
        </Select>

        <Select options={options} name="m" size="m">
          Medium
        </Select>

        <Select options={options} name="l" size="l">
          Large
        </Select>

        <Select options={options} name="xl" size="xl">
          Extra large
        </Select>
      </form>
    </FormProvider>
  )
}

export const Disabled = () => {
  const methods = useForm()

  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option" disabled>
          Disabled select
        </Select>
      </form>
    </FormProvider>
  )
}

export const DisabledOption = () => {
  const methods = useForm()

  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres', isDisabled: true },
  ]

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option">
          Disabled select
        </Select>
      </form>
    </FormProvider>
  )
}

export const Multi = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      option: [{ value: 'one', label: 'Uno' }],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option" isMulti />
      </form>
    </FormProvider>
  )
}

export const MultiWithDisabledOption = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres', isDisabled: true },
  ]

  const methods = useForm({
    defaultValues: {
      option: [{ value: 'one', label: 'Uno' }],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option" isMulti />
      </form>
    </FormProvider>
  )
}

export const ObjectsAsValues = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      option: { value: { english: 'two' }, label: 'Dos' },
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option" />
      </form>
    </FormProvider>
  )
}

export const MultiObjectAsValue = () => {
  const options = [
    { value: { english: 'one' }, label: 'Uno' },
    { value: { english: 'two' }, label: 'Dos' },
    { value: { english: 'three' }, label: 'Tres' },
  ]

  const methods = useForm({
    defaultValues: {
      option: [{ value: { english: 'two' }, label: 'Dos' }],
    },
  })

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(() => {})}>
        <Select options={options} name="option" isMulti />
      </form>
    </FormProvider>
  )
}

export const SelectReactHookForms = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  const methods = useForm()

  return (
    <FormProvider {...methods}>
      <form>
        <p>
          Using `SelectReactHookForm` directly, which allows you to use the
          component without a label and feedback.
        </p>
        <label>
          Label
          <SelectReactHookForm
            id="option"
            name="option"
            placeholder="Selecteer optie"
            options={options}
          />
        </label>
      </form>
    </FormProvider>
  )
}

export const ReactSelectComponent = () => {
  const options = [
    { value: 'one', label: 'Uno' },
    { value: 'two', label: 'Dos' },
    { value: 'three', label: 'Tres' },
  ]

  return (
    <form>
      <p>
        Not using React Hook Form? <code>SelectComponent</code> gives you access
        to the dropdown without React Hook Form integration. The React Select
        package is used as foundation. Check out{' '}
        <a
          href="https://react-select.com/props"
          target="_blank"
          rel="noreferrer"
        >
          their website
        </a>{' '}
        to see what additional props you can pass.
      </p>
      <label>
        Label
        <SelectComponent
          id="option"
          name="option"
          placeholder="Selecteer optie"
          options={options}
        />
      </label>
    </form>
  )
}
