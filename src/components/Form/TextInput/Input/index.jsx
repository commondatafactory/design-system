// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool, element, func, node, object, oneOf, oneOfType } from 'prop-types'
import Icon from '../../../Icon'
import { StyledInput, StyledIcon, StyledButton, Wrapper } from './index.styles'

const Input = React.forwardRef(
  ({ icon, iconAtEnd, handleIconAtEnd, ...props }, ref) => (
    <>
      {icon || iconAtEnd ? (
        <Wrapper>
          {icon && <StyledIcon as={icon} />}
          <StyledInput
            ref={ref}
            {...props}
            $hasIcon={!!icon}
            $handleIconAtEnd={handleIconAtEnd}
          />
          {iconAtEnd && (
            <StyledButton
              type="button"
              onClick={handleIconAtEnd}
              title="Execute input button"
            >
              <Icon as={iconAtEnd} />
            </StyledButton>
          )}
        </Wrapper>
      ) : (
        <StyledInput
          ref={ref}
          {...props}
          $hasIcon={!!icon}
          $handleIconAtEnd={handleIconAtEnd}
        />
      )}
    </>
  ),
)

Input.propTypes = {
  disabled: bool,
  handleIconAtEnd: func,
  icon: oneOfType([element, node, object, func]),
  iconAtEnd: oneOfType([element, node, object, func]),
  size: oneOf(['xs', 's', 'm', 'l', 'xl', 'fullWidth']),
}

Input.defaultProps = {
  size: 'm',
}

export default Input
