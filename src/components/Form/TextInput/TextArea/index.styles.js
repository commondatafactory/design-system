// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { inputStyles } from '../Input/index.styles'

export const StyledTextArea = styled.textarea`
  ${inputStyles}
  height: 6.1rem;
`
