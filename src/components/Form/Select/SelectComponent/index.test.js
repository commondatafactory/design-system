// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useRef } from 'react'
import { render, fireEvent } from '@testing-library/react'
import selectEvent from 'react-select-event'
import TestThemeProvider from '../../../../themes/TestThemeProvider'
import SelectComponent from './index'

const options = [
  { value: 'one', label: 'Uno' },
  { value: 'two', label: 'Dos' },
  { value: 'three', label: 'Tres' },
]

test('selecting an option should update the form values', async () => {
  const { getByTestId, getByLabelText } = render(
    <TestThemeProvider>
      <form data-testid="form">
        <label htmlFor="selection">Choose</label>
        <SelectComponent
          options={options}
          name="selection"
          inputId="selection"
          defaultValue={options[2]}
        />
      </form>
    </TestThemeProvider>,
  )

  const form = getByTestId('form')
  expect(form).toHaveFormValues({ selection: 'three' })

  await selectEvent.select(getByLabelText('Choose'), ['Dos'])

  expect(form).toHaveFormValues({ selection: 'two' })
})

test('the ref should be passed to select element', async () => {
  const TestForm = () => {
    const select = useRef()
    return (
      <TestThemeProvider>
        <form data-testid="form" onReset={() => select.current.clearValue()}>
          <label htmlFor="selection">Choose</label>
          <SelectComponent
            ref={select}
            options={options}
            name="selection"
            inputId="selection"
          />
        </form>
      </TestThemeProvider>
    )
  }
  const { getByTestId, getByLabelText } = render(<TestForm />)

  await selectEvent.select(getByLabelText('Choose'), ['Dos'])

  const form = getByTestId('form')
  expect(form).toHaveFormValues({ selection: 'two' })

  fireEvent.reset(form)

  expect(form).toHaveFormValues({ selection: '' })
})
