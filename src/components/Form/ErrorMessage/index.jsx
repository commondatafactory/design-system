// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node } from 'prop-types'
import styled from 'styled-components'
import Icon from '../../Icon'

const IconError = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    fill="currentcolor"
    {...props}
  >
    <path d="M16 16H0V0h16v16zm-9-5v2h2v-2H7zm0-8v6h2V3H7z" />
  </svg>
)

const StyledIconError = styled(IconError)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
  vertical-align: text-top;
  fill: ${(p) => p.theme.tokens.colorError};
`

export const StyledErrorMessage = styled.p`
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  color: ${(p) => p.theme.tokens.colorError};
  margin: ${(p) => p.theme.tokens.spacing03} 0
    ${(p) => p.theme.tokens.spacing06} 0;
`

const ErrorMessage = ({ children, ...props }) => (
  <StyledErrorMessage {...props}>
    <Icon as={StyledIconError} />
    {children}
  </StyledErrorMessage>
)

ErrorMessage.propTypes = {
  children: node,
}

export default ErrorMessage
