// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

import ErrorMessage from './index'

const ErrorMessageStory = {
  title: 'Components/Form/ErrorMessage',
  parameters: {
    componentSubtitle:
      'Error message for form elements. Should be used to display feedback about invalid user input.',
  },
  component: ErrorMessage,
}

export default ErrorMessageStory

export const Intro = () => {
  return (
    <ErrorMessage>You cannot choose more than 3 pizza toppings</ErrorMessage>
  )
}
