// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

const Content = styled.div`
  margin-top: 2.5rem;
`

export default Content
