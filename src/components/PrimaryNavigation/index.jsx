// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect, useMemo } from 'react'
import { arrayOf, elementType, shape, string } from 'prop-types'
import debounce from '../../utils/debounce'
import breakpoints from '../../themes/parts/breakpoints'
import DesktopNavigation from './DesktopNavigation'
import MobileNavigation from './MobileNavigation'

const PrimaryNavigation = (props) => {
  const [isMobile, setIsMobile] = useState(false)

  const resizeHandler = useMemo(() => {
    return debounce(() => {
      setIsMobile(window.innerWidth < breakpoints.md)
    })
  }, [])

  useEffect(() => {
    window && window.addEventListener('resize', resizeHandler)
    resizeHandler()

    return () => {
      window && window.removeEventListener('resize', resizeHandler)
    }
  }, [resizeHandler])

  return isMobile ? (
    <MobileNavigation {...props} />
  ) : (
    <DesktopNavigation {...props} />
  )
}

PrimaryNavigation.propTypes = {
  LinkComponent: elementType.isRequired,
  items: arrayOf(
    shape({
      name: string.isRequired,
      Icon: elementType,
      to: string.isRequired,
    }),
  ).isRequired,
  ariaLabel: string,
  ariaLabelSubNavigation: string,
  mobileMoreText: string.isRequired,
  pathname: string.isRequired,
}

PrimaryNavigation.defaultProps = {
  ariaLabel: 'Hoofdnavigatie',
  ariaLabelSubNavigation: 'Subnavigatie',
}

export default PrimaryNavigation

export { mobileNavigationHeight } from './MobileNavigation/components/MobileNavMenu/index.styles'
