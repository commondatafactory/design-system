// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import 'cypress-axe'

const axeRunOptions = {
  runOnly: ['best-practice', 'wcag2a', 'wcag21a'],
}

const terminalLog = (violations) => {
  cy.task(
    'log',
    `${violations.length} accessibility violation${
      violations.length === 1 ? '' : 's'
    } ${violations.length === 1 ? 'was' : 'were'} detected`,
  )
  // pluck specific keys to keep the table readable
  const violationData = violations.map(
    ({ id, impact, description, nodes }) => ({
      id,
      impact,
      description,
      nodes: nodes.length,
    }),
  )

  cy.task('table', violationData)
}

export { axeRunOptions, terminalLog }
